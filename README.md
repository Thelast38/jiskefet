## Project: "Open International Project "jiskefet". API development for making Tech work-shift reports with a template"

First version of the project which is using libraries such as express for server and pdfmake for generating pdf from json format.

## Participants
| Study group | Username | Fullname |
|----------------|------------------|--------------------------| 
| 181-351 | @Shadthrough | Bogachev M. G. |
| 181-351 | @TheLast38 | Semennikov K.V. |
| 181-351 | @Anton250 | Petrov A.D. |

## Participants' personal contribution

### Bogachev M. G.

- Developed site design;
- Made PDF-report templates;

### Semennikov K.V.

- Wrote a part of the server using Express and set html-pdf;
- Made an API-Request;

### Petrov A.D.

- Wrote a part of the server using Express and set html-pdf;
- Made a connection between API-Request and html;

## Проект: «Международный открытый проект jiskefet. Разработка API для создания отчётов технических смен по шаблону»

Первая версия использует библиотеки express для сервера и pdfmake для генерации pdf из json формата.

## Участники

| Учебная группа | Имя пользователя | ФИО                      |
|----------------|------------------|--------------------------|
| 181-351       | @Shadthrough       | Богачев М.Г.              |
| 181-351        | @TheLast38       | Семенников К.В.              |
| 181-351        | @Anton250      | Петров А.Д. |

## Личный вклад участников

### Богачев М.Г.

- Разработал дизайн сайта;
- Cоздал шаблоны для pdf-отчётов;

### Семенников К.В.

- Написал часть сервера с использованием express и настроил html-pdf;
- Сделал API запрос;

### Петров А.Д.

- Написал часть сервера с использованием express и настроил html-pdf;
- Соединил API запрос и html;