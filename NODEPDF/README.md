1. Installing nodejs. If you have already installed nodejs on your machine, skip this.
+ ```$ sudo apt update ```
+ ```$ sudo apt install build-essential checkinstall ```
+ ```$ sudo apt install libssl-dev```
+ ```$ wget -qO- https://raw.githubusercontent.com/creationix/nvm/v0.33.8/install.sh | bash```
+ restart the terminal
+ check available versions via ```$ nvm ls-remote```
+ install needed version for example 10.15.2 via ```$ nvm install 10.15.2```
+ use the installed version via ```$ nvm use 10.15.2```
+ to set default nodejs version write ```$ nvm alias default 10.15.2```
+ also you can check installed versions of nodejs via ```$ nvm list```
+ Useful notes:
  + you can delete some versions: first of all you need to turn off version via ```$ nvm deactivate 10.15.2```
  + then ```$ nvm uninstall 10.15.2```
2. clonning jiskefetPDF
+ ```$ git clone https://gitlab.com/Thelast38/jiskefet```
3. deploying server
+ ```$ cd jiskefet/NODEPDF```
+ ```$ npm install```
+ ```$ git clone git clone --branch 0.1 https://github.com/bpampuch/pdfmake.git```
+ ```$ cd pdfmake/```
+ ```$ npm install```
+ ```$ npm install gulp -g```
+ ```$ gulp buildFonts```
+ ```$ cd ..```
4. running server
+ ```$ node app```
+ the server is running in 3010 port
+ when you have closed the terminal server will shut down. To forbid him shutting down after ```$ node app``` you need to write ```$ disown -h %1``` and then ```$ bg 1```, now the srever will not shut down when ypu close the terminal
+ to stop the server you need to write ```$ killall node```
5. Everytime when you restart the terminal, you need to write ```$ nvm use 10.15.2``` or your version nodejs

