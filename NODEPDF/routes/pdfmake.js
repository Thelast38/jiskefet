

const express = require('express');
const router = express.Router();


const pdfMake = require('../pdfmake/build/pdfmake');

const pdfFonts = require('../pdfmake/build/vfs_fonts');

pdfMake.vfs = pdfFonts.pdfMake.vfs;

const date = new Date();
const day = date.getDate();
const month = date.getMonth() + 1;
const year = date.getFullYear();
const hour = date.getHours();
const min = date.getMinutes();
const allDate = String(hour) + ":" + String(min) + " " + String(day) + "." + String(month) + "." + String(year);




router.post('/pdf', function(req, res, next){

  const fname = req.body.fname;
  const lname = req.body.lname;
  const post = req.body.occupation;
  const bugReport = req.body.bugReport;

  var documentDefine = {
content: [
  {text:'CERN ALICE',
    fontSize:40,
    color:'blue',
    alignment: 'center'
  },

  '\n\n',
  {text:[{text:'First name - ',fontSize:16},
  fname]},
  {text:[{text:'Second name - ',fontSize:16},
  lname]},
  {text:[{text:'Post - ',fontSize:16},
  post]},
  {
  text: 'Bug-report',
  style: 'header'
  },
  {
  text:bugReport,
  style: 'bug'
  },
  {text:"Data: {date}".replace("{date}", allDate),
  margin:[0,25]
  }

  ],
  styles: {
  header: {
  fontSize: 21,
  bold: true,
  alignment: 'center'
  },
  bug:{
  margin:[0,5,5,5],
  color:'red'
  }
}

}

  const pdfDoc = pdfMake.createPdf(documentDefine);
  pdfDoc.getBase64(function(data){
    console.log(allDate);

    res.writeHead(200, {
      "Content-type": "application/pdf",
      "Content-Disposition": 'attachment;filename="BugReport_{date}.pdf"'.replace("{date}", allDate)
    });

    const download = Buffer.from(data.toString("utf-8"), "base64");
    res.end(download);
  });

});

module.exports = router;
